<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->integer('base_price');
            $table->integer('final_price')->nullable();
            $table->integer('count');
            $table->integer('discount_amount')->nullable();
            $table->integer('discount_percent')->nullable();
            $table->boolean('isSell');
            $table->boolean('isSpecialCase');
            $table->boolean('isSend');
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employee'); 
            $table->foreign('customer_id')->references('id')->on('customer'); 
            $table->foreign('product_id')->references('id')->on('product'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
};
