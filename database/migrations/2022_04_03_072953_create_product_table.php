<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('sku');
            $table->text('desc');
            $table->integer('length')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->float('volmetric')->nullable();
            $table->integer('price');
            $table->integer('agen_price');
            $table->integer('reseller_price');
            $table->integer('offline_price');
            $table->integer('handling_fee');
            $table->integer('discount')->nullable();
            $table->integer('stock');
            $table->integer('stock_offline');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
};
