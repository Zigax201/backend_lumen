<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_product_transaction', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('transaction_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->integer('base_price');
            $table->integer('final_price')->nullable();
            $table->float('volmetric')->nullable();
            $table->integer('handling_fee');
            $table->integer('count');
            $table->boolean('isSell');
            $table->boolean('isSpecialCase');
            $table->timestamps();

            $table->foreign('transaction_id')->references('id')->on('transaction'); 
            $table->foreign('product_id')->references('id')->on('product'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_product_transaction');
    }
};
