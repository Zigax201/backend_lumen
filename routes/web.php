<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('register', 'AuthController@register_employee');
    $router->post('login', 'AuthController@login_employee');
    $router->get('user', 'AuthController@user');
});

$router->group(['prefix' => 'transaction', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'TransactionController@index');
    $router->get('/all', 'TransactionController@show_all');
    $router->post('/add', 'TransactionController@store');
});

$router->get('products', 'ProductController@index');
$router->get('product/{sku}', 'ProductController@show');

$router->group(['prefix' => 'product', 'middleware' => 'auth'], function () use ($router) {
    $router->post('/', 'ProductController@store');
    $router->put('/{sku}', 'ProductController@update');
    $router->delete('/{sku}', 'ProductController@delete');
});

$router->group(['prefix' => 'cart', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/all', 'CartController@index');
    $router->get('/employee', 'CartController@showByEmployee');
    $router->get('/customer/{customer_id}', 'CartController@showByCustomer');
    $router->post('/send', 'CartController@send');
    $router->post('/', 'CartController@store');
    $router->put('/', 'CartController@update');
    $router->delete('/', 'CartController@delete');
});

$router->group(['prefix' => 'customer', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/all', 'CustomerController@index');
    $router->get('/phone', 'CustomerController@showByPhone');
    $router->get('/name', 'CustomerController@showByName');
    $router->post('/', 'CustomerController@store');
    $router->put('/', 'CustomerController@update');
    $router->delete('/', 'CustomerController@delete');
});
