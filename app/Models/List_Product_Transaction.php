<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class List_Product_Transaction extends Model
{
    public $table = 'list_product_transaction'; 
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'transaction_id', 
        'product_id', 
        'base_price', 
        'final_price', 
        'volmetric',
        'handling_fee', 
        'count', 
        'isSell',
        'isSpecialCase'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
