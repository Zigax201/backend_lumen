<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $table = 'transaction'; 
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'customer_id', 
        'employee_id', 
        'number', 
        'total_price', 
        'order_price',
        'payment_status', 
        'payment_method', 
        'order_status', 
        'isSell'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function list_product(){
        return $this->hasMany(List_Product_Transaction::class);
    }
}
