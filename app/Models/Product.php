<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'product';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title', 
        'sku', 
        'desc', 
        'length', 
        'width', 
        'height',
        'volmetric', 
        'price', 
        'offline_price', 
        'agen_price', 
        'reseller_price',
        'handling_fee', 
        'discount', 
        'stock', 
        'stock_offline'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [];

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function list_product_transaction()
    {
        return $this->hasMany(List_Product_Transaction::class);
    }

}
