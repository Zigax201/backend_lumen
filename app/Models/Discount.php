<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model 
{
    public $table = 'discount'; 
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name', 'discount'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [
        'timestamp'
    ];
}
