<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function index()
    {
        if (Auth::user()->rule > 2) {
            return Cart::paginate(10);
        } else {
            return response()->json('Unathorized', 401);
        }
    }

    public function showByEmployee()
    {
        return Auth::user()->cart;
    }

    public function showByCustomer($customer_id)
    {
        $cart = Cart::where('customer_id', $customer_id)->first();
        if (empty($cart)) {
            return response()->json(['Message' => 'Cart with this customer is not exist'], 404);
        }
        return response()->json($cart, 200);
    }

    public function store(Request $req)
    {
        try {
            $validate = $this->validate($req, [
                'customer_id' => 'required',
                'product_id' => 'required',
                'employee_id' => 'required',
                'base_price' => 'required',
                'count' => 'required',
                'isSell' => 'required',
                'isSpecialCase' => 'required'
            ]);

            $check_cart = Cart::where('employee_id', $validate['employee_id'])
                ->where('customer_id', $validate['customer_id'])
                ->where('product_id', $validate['product_id'])
                ->where('isSell', $validate['isSell'])
                ->where('isSpecialCase', $validate['isSpecialCase'])->first();

            if (!empty($check_cart)) {
                $check_cart->count = $validate['count'];

                if ($check_cart->discount_amount == null && $check_cart->discount_percent != null) {
                    $discount = (float) ($check_cart->discount_percent / 100);
                    $check_cart->final_price = $check_cart->count * $check_cart->base_price * $discount;
                    error_log(1);
                } else if ($check_cart->discount_percent == null && $check_cart->discount_amount != null) {
                    $check_cart->final_price = $check_cart->count * $check_cart->base_price - $check_cart->discount_amount;
                    error_log(2);
                } else {
                    $check_cart->final_price = $check_cart->count * $check_cart->base_price;
                    error_log(3);
                }

                return response()->json($check_cart, 200);
            }

            // $check_cart = Cart::where('employee_id', $validate['employee_id'])
            //     ->where('customer_id', $validate['customer_id'])
            //     ->where('product_id', $validate['product_id'])->get();

            // foreach ($check_cart as $key) {
            //     if (empty($key)) {
            //         if($key->isSell == $validate['isSell'] && $key->price == $validate['price']){
            //             $key->count += $validate['count'];
            //             $key->save(); 
            //             return response()->json($key, 200);
            //         }
            //     }
            // }

            $cart = new Cart($validate);
            
            if ($cart->discount_amount == null && $cart->discount_percent != null) {
                $discount = (float) ($cart->discount_percent / 100);
                $cart->final_price = $cart->count * $cart->base_price * $discount;
                error_log(1);
            } else if ($cart->discount_percent == null && $cart->discount_amount != null) {
                $cart->final_price = $cart->count * $cart->base_price - $cart->discount_amount;
                error_log(2);
            } else {
                $cart->final_price = $cart->count * $cart->base_price;
                error_log(3);
            }
            
            $cart->save();

            $product = Cart::find($cart->id)->product;

            $product->stok_offline -= $validate['count'];

            return response()->json($cart, 200);
        } catch (\Throwable $th) {
            error_log($th);
            return response()->json(['Message' => 'fail add to cart'], 500);
        }
    }

    public function update(Request $req)
    {
        try {
            $validate = $this->validate($req, [
                'customer_id' => 'required',
                'product_id' => 'required',
                'employee_id' => 'required',
                'base_price' => 'required',
                'count' => 'required',
                'isSell' => 'required',
                'isSpecialCase' => 'required'
            ]);

            $check_cart = Cart::where('employee_id', $validate['employee_id'])
                ->where('customer_id', $validate['customer_id'])
                ->where('product_id', $validate['product_id'])
                ->where('isSell', $validate['isSell'])
                ->where('isSpecialCase', $validate['isSpecialCase'])->first();

            if (empty($check_cart)) {
                return response()->json(['Message' => 'Cart does not exist, please add a new cart'], 404);
            }

            $check_cart->count = $validate['count'];
            
            $product = Cart::find($check_cart->id)->product;
            
            if ($check_cart->count > $validate['count']) {
                $product->stok_offline -= $check_cart->count;
            } else {
                $product->stok_offline -= $check_cart->count;
            }
            
            if ($check_cart->discount_amount == null) {
                $discount = (float) ($check_cart->discount_percent / 100);
                $check_cart->final_price = $check_cart->count * $check_cart->base_price * $discount;
            } else if ($check_cart->discount_percent == null) {
                $check_cart->final_price = $check_cart->count * $check_cart->base_price - $check_cart->discount_amount;
            } else {
                $check_cart->final_price = $check_cart->count * $check_cart->base_price;
            }
            
            $check_cart->save();
            
            $product->save();

            return response()->json($check_cart, 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['Message' => 'fail update cart'], 500);
        }
    }

    public function delete(Request $req)
    {
        try {
            $validate = $this->validate($req, [
                'customer_id' => 'required',
                'product_id' => 'required',
                'employee_id' => 'required',
                'base_price' => 'required',
                'count' => 'required',
                'isSell' => 'required',
                'isSpecialCase' => 'required'
            ]);

            $check_cart = Cart::where('employee_id', $validate['employee_id'])
                ->where('customer_id', $validate['customer_id'])
                ->where('product_id', $validate['product_id'])
                ->where('isSell', $validate['isSell'])
                ->where('isSpecialCase', $validate['isSpecialCase'])->first();

            if (empty($check_cart)) {
                return response()->json(['Message' => 'Cart does not exist, please add a new cart'], 404);
            }

            $check_cart->delete();
            return response()->json(['Message' => 'Cart deleted']);
        } catch (\Throwable $th) {
            // error_log($th);
            return response()->json(['Message' => 'fail delete cart'], 500);
        }
    }
    
    public function send(Request $req){
        try {
            $cart = Cart::where('customer_id', $req->customer_id);
            $cart->isSend = true;
            $cart->save();
            return response()->json('Success', 200);
        } catch (\Throwable $th) {
            // error_log($th);
            return response()->json('Failed', 500);
        }
    }
}
