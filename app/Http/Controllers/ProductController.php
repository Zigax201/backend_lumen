<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function index()
    {
        return response()->json(Product::paginate(20));
    }

    public function store(Request $req)
    {
        if (Auth::user()->rule == 4) {

            $validate = $this->validate($req, [
                'title' => 'required',
                'sku' => 'required',
                'desc' => 'required',
                'length' => 'required',
                'width' => 'required',
                'height' => 'required',
                'volmetric' => 'required',
                'price' => 'required',
                'offline_price' => 'required',
                'agen_price' => 'required',
                'reseller_price' => 'required',
                'handling_fee' => 'required',
                'stock' => 'required',
                'stock_offline' => 'required'
            ]);

            if (!$validate) {
                return response()->json(['Message' => 'Input is not valid'], 400);
            }
            
            if(Product::where('sku', $req->sku)->count() > 0){
                return response()->json(['Message' => 'SKU already exist'], 404);
            }
            
            try {
                $product = new Product($validate);
                $product->save();
                return response()->json(['Message' => 'Product saved', 'product' => $product], 200);
            } catch (\Throwable $th) {
                // error_log($th);
                return response()->json(['Message' => 'Failed to save product'], 500);
            }
        } else {
            return response()->json('Unathorized', 401);
        }
    }
    
    public function update(Request $req, $sku)
    {
        if (Auth::user()->rule == 4) {
            
            $product = Product::where('sku', $sku)->first();
            
            if(empty($product)){
                return response()->json(['Message' => 'Product does not exist'], 404);
            }

            $validate = $this->validate($req, [
                'title' => 'required',
                'sku' => 'required',
                'desc' => 'required',
                'length' => 'required',
                'width' => 'required',
                'height' => 'required',
                'volmetric' => 'required',
                'price' => 'required',
                'offline_price' => 'required',
                'agen_price' => 'required',
                'reseller_price' => 'required',
                'handling_fee' => 'required',
                'stock' => 'required',
                'stock_offline' => 'required'
            ]);

            if (!$validate) {
                return response()->json(['Message' => 'Input is not valid'], 400);
            }

            try {

                $product->title = $validate['title'];
                $product->sku = $validate['sku'];
                $product->desc = $validate['desc'];
                $product->length = $validate['length'];
                $product->width = $validate['width'];
                $product->height = $validate['height'];
                $product->volmetric = $validate['volmetric'];
                $product->price = $validate['price'];
                $product->offline_price = $validate['offline_price'];
                $product->agen_price = $validate['agen_price'];
                $product->reseller_price = $validate['reseller_price'];
                $product->handling_fee = $validate['handling_fee'];
                $product->stock = $validate['stock'];
                $product->stock_offline = $validate['stock_offline'];

                $product->save();

                return response()->json(['Message' => 'Product saved', 'product' => $product], 200);
            } catch (\Throwable $th) {
                error_log($th);
                return response()->json(['Message' => 'Failed to save product'], 500);
            }
        } else {
            return response()->json('Unathorized', 401);
        }
    }
    
    public function show($sku)
    {
        return Product::where('sku', "LIKE", "%$sku%")->paginate(10);
    }
    
    public function delete($sku)
    {
        try {
            $product = Product::where('sku', $sku)->first();
            if (empty($product)){
                return response()->json(['Message' => 'Product does not exist'], 404);
            }
            $product->delete();
            return response()->json(['Message' => 'Product deleted'], 200);
        } catch (\Throwable $th) {
            return response()->json(['Message' => 'failed delete product'], 500);
            //throw $th;
        }
    }
}
