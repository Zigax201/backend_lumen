<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Employee;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register_employee(Request $req)
    {
        $validate = $this->validate($req, [
            'name' => 'required|max:255',
            'password' => 'required',
            'rule' => 'required'
        ]);

        if ($validate) {
            $employee = new Employee();
            $employee->name = $validate['name'];
            $employee->password = Hash::make($validate['password']);
            $employee->rule = $validate['rule'];
            $employee->save();

            return response()->json($employee, 201);
        }

        return response()->json(['Message' => 'input is not valid please try again'], 500);
    }

    public function login_employee(Request $req)
    {
        $validate = $this->validate($req, [
            'name' => 'required|exists:employee,name',
            'password' => 'required'
        ]);

        if ($validate) {

            $employee = Employee::where('name', $validate['name'])->first();

            if (!Hash::check($validate['password'], $employee->password)) {
                return abort(401, "email or password not valid");
            }

            $payload = [
                'iat' => intval(microtime(true)),
                'exp' => intval(microtime(true)) * (60 * 60 * 10000), 
                'uid' => $employee->id
            ];

            $token = JWT::encode($payload, env('JWT_SECRET'), 'HS256'); // HS256 merupakan salah satu algoritma jwt, dapat di ganti menyesuaikan apps

            return response()->json(['Message' => 'Log in Success, Welcome ' . $employee->name, 'access_token' => $token]);
        }

        return response()->json(['Message' => 'input is not valid please try again'], 400);
    }

    public function user(){
        return response()->json(Auth::user(), 200);
    }
}
