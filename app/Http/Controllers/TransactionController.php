<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Cart;
use App\Models\List_Product_Transaction;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function index()
    {
        return response()->json(Auth::user()->transaction);
    }

    public function show_all()
    {
        if (Auth::user()->rule == 4) {
            $transactions = Transaction::all()->list_product; 
            return response()->json($transactions);
        }
        return response()->json('Unauthorized', 401);
    }

    public function store(Request $req)
    {
        try {
            if (Auth::user()->rule > 2) {
                $validate = $this->validate($req, [
                    'customer_id' => 'required',
                    'employee_id' => 'required',
                    'number' => 'required',
                    'total_price' => 'required',
                    'order_price' => 'required',
                    'payment_status' => 'required',
                    'payment_method' => 'required',
                    'order_status' => 'required',
                    'isSell' => 'required'
                ]);

                $transaction = new Transaction($validate);
                $transaction->save();

                if($transaction != null){
                    $cart = Cart::where('customer_id', $transaction->customer_id)->get();

                    foreach ($cart as $key) {
                        $list_product_transaction = new List_Product_Transaction($cart);
                        
                        $product = Product::find($key->product_id);

                        $list_product_transaction->transaction_id = $transaction->id;
                        $list_product_transaction->volmetric = $product->volmetric;
                        $list_product_transaction->handling_fee = $product->handling_fee;

                        $list_product_transaction->save();
                    }
                }
                return response()->json(['Message' => 'Transaction Success', 'Transaction' => $transaction], 200);
            }
        } catch (\Throwable $th) {
            return response()->json(['Message' => 'Transaction failed'], 500);
        }
    }

}
