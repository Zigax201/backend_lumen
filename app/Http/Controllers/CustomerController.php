<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function index()
    {
        return Customer::paginate(20);
    }

    public function showByPhone(Request $req)
    {
        return Customer::where('phone', "LIKE", "%$req->phone%")->get();
    }

    public function showByName(Request $req)
    {
        return Customer::where('name', "LIKE", "%$req->name%")->get();
    }

    public function store(Request $req)
    {
        try {
            $validate = $this->validate($req, [
                'name' => 'required',
                'phone' => 'required',
                'customer_type' => 'required'
            ]);
    
            if (Customer::where('phone', $validate['phone'])->first() != null) {
                return response()->json(['Message' => 'Phone number already exist']);
            }
    
            $customer = new Customer($validate);
            $customer->save();
            return response()->json(['Message' => 'Success add customer', 'Customer' => $customer], 200);
        } catch (\Throwable $th) {
            // error_log($th);
            return response()->json(['Message' => 'Failed add customer'], 500);
        }
    }

    public function update(Request $req)
    {
        try {
            $validate = $this->validate($req, [
                'name' => 'required',
                'phone' => 'required',
                'customer_type' => 'required'
            ]);
            
            $customer = Customer::where('phone', $validate['phone'])->first();

            if ($customer == null) {
                return response()->json(['Message' => 'Customer does not exist']);
            }
    
            $customer->name = $validate['name'];
            $customer->phone = $validate['phone'];
            $customer->customer_type = $validate['customer_type'];
            $customer->save();
            return response()->json(['Message' => 'Success update customer', 'Customer' => $customer], 200);
        } catch (\Throwable $th) {
            return response()->json(['Message' => 'Failed update customer'], 500);
        }
    }

    public function delete(Request $req)
    {
        try {
            $validate = $this->validate($req, [
                'id' => 'required'
            ]);
            
            $customer = Customer::find($validate['id']);

            if ($customer == null) {
                return response()->json(['Message' => 'Customer does not exist']);
            }

            $customer->delete();
            return response()->json(['Message' => 'Customer deleted'], 200);
        } catch (\Throwable $th) {
            return response()->json(['Message' => 'Failed delete customer'], 500);
        }
    }
}
